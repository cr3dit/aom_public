package com.cr3dit.alen.aom.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.fragments.BasketFragment;
import com.cr3dit.alen.aom.fragments.MainFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    Drawer result;
    SharedPreferences accountPref;
    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    if (savedInstanceState == null) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.MainFrame, MainFragment.newInstance(), "Main Fragment")
                .commit();
    }

        accountPref = getApplicationContext().getSharedPreferences("MyPref", 0);

    Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);

    setSupportActionBar(myToolbar);

    createDrawer(myToolbar,accountPref.getBoolean("isLoggedIn",false));


}


public void createDrawer(Toolbar myToolbar, final boolean loggedIn){
    // Create the AccountHeader
    AccountHeader headerResult = new AccountHeaderBuilder()
            .withActivity(this)
            .withHeaderBackground(R.drawable.drawerbg)
            .addProfiles(
                    new ProfileDrawerItem().withName(accountPref.getString("name","")).withEmail(accountPref.getString("email","")).withIcon(getResources().getDrawable(R.drawable.icon))
            ).build();

        //https://github.com/mikepenz/MaterialDrawer
    //add drawer items
    final PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName("Home");
    SecondaryDrawerItem logStat;
    if (loggedIn) {
        logStat = new SecondaryDrawerItem().withIdentifier(2).withName("Log out");
    }
    else
    {
        logStat = new SecondaryDrawerItem().withIdentifier(2).withName("Log in");

    }

    //create the drawer and remember the `Drawer` result object
     result = new DrawerBuilder()
            .withActivity(this).withAccountHeader(headerResult)
            .withToolbar(myToolbar)
            .addDrawerItems(
                    item1,
                    new DividerDrawerItem(),logStat)
            .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {

                @Override
                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {


                  if(drawerItem.getIdentifier()==1){

                          finish();
                          startActivity(getIntent());
                          result.closeDrawer();
                    }
                  else if(drawerItem.getIdentifier()==2){
                    if(loggedIn) {
                                    startActivity(new Intent(getApplicationContext(), LogoutActivity.class));

                                }
                    else
                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class).putExtra("fromActivity",true));;

                                    result.closeDrawer();

                    }
                  else if(drawerItem.getIdentifier()==3){

                      startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                      result.closeDrawer();

                  }

                    return true;
                }
            })

            .build();
    //initialize and create the image loader logic
    DrawerImageLoader.init(new AbstractDrawerImageLoader() {
        @Override
        public void set(ImageView imageView, Uri uri, Drawable placeholder) {
            Picasso.with(imageView.getContext()).load(accountPref.getString("imageUrl","")).placeholder(placeholder).into(imageView);
        }

        @Override
        public void cancel(ImageView imageView) {
            Picasso.with(imageView.getContext()).cancelRequest(imageView);
        }

    /*
    @Override
    public Drawable placeholder(Context ctx) {
        return super.placeholder(ctx);
    }

    @Override
    public Drawable placeholder(Context ctx, String tag) {
        return super.placeholder(ctx, tag);
    }
    */
    });



    result.closeDrawer();


}


 @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;

            case R.id.action_basket:
                getSupportFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.MainFrame, new BasketFragment(), "BasketFrag")
                        .commit();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() >= 0) {
            getFragmentManager().popBackStack();
        } else {
        }
        super.onBackPressed();

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

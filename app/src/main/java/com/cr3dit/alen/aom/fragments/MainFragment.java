package com.cr3dit.alen.aom.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.controller.DatabaseHelper;
import com.cr3dit.alen.aom.controller.MenuAdapter;
import com.cr3dit.alen.aom.model.Entrepreneur;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {
    DatabaseHelper dbHelper;
    List<Entrepreneur> dataCollection;
    SharedPreferences pref;
    MenuAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dbHelper = new DatabaseHelper(getActivity());

        RecyclerView menuRecycler = (RecyclerView) inflater.inflate(R.layout.fragment_main, container, false);

        dataCollection = dbHelper.getAllEntrepreneurs();

        int articleSize = dataCollection.size();

        final String[] entrepreneurName = new String[articleSize];
        final String[] entrepreneurId = new String[articleSize];
        final String[] entrepreneurPic = new String[articleSize];

        int counter = 0;

        for (Entrepreneur entrepreneurs : dataCollection) {
            entrepreneurName[counter] = entrepreneurs.getEntrepreneurName();
            entrepreneurId[counter] = entrepreneurs.get_idEntrepreneur();
             entrepreneurPic[counter] = entrepreneurs.getEntPic();

            counter++;


        }
        adapter = new MenuAdapter(getActivity(),entrepreneurName, entrepreneurPic);
        menuRecycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        menuRecycler.setLayoutManager(layoutManager);
        final FragmentTransaction ft = getFragmentManager().beginTransaction();


        adapter.setListener(new MenuAdapter.Listener() {
            public void onClick(int position) {
                {


                    pref = getActivity().getPreferences(0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("EntrepreneurClicked", (entrepreneurId[position]));
                    editor.apply();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    ft.addToBackStack(null);
                    ft.replace(R.id.MainFrame, new EntrepreneurDetailFragment(), "NewFragmentTag");
                    ft.commit();


                }


            }
        });

        return menuRecycler;

    }


}



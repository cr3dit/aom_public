package com.cr3dit.alen.aom.controller;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;


/**
 * Created by alen on 08.01.2017..
 */

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.ViewHolder> {

    private String[] articleNames;
    private float[] price;
    private float[]quantity;
    private Listener listener;

    public interface Listener {
        void onClick(int position);

    }
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    //constructor for 2 variables
    public BasketAdapter(String[] articleNames, float[] price,float[]quantity) {
        this.articleNames = articleNames;
        this.price = price;
        this.quantity=quantity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_placeholder_basket_articles, parent, false);
        return new ViewHolder(cv);
    }

    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        TextView textView = (TextView) cardView.findViewById(R.id.txt_articleName);
        textView.setText(articleNames[position]);

        TextView textView2 = (TextView) cardView.findViewById(R.id.txt_price);
        textView2.setText(String.format("%.2f",price[position]*quantity[position])+"");

        TextView txtQuantity=(TextView)cardView.findViewById(R.id.txt_quantity);
        txtQuantity.setText(String.format("%.0f",quantity[position])+"");


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);


                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return articleNames.length;
    }
}
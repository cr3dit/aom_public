package com.cr3dit.alen.aom.controller;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by alen on 24.04.2017..
 */

public class JSONParser {


    public String jsonData(String passedUrl) {
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(passedUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            StringBuilder sb = new StringBuilder();

            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String json;
            while ((json = bufferedReader.readLine()) != null) {
                sb.append(json + "\n");
            }




            return sb.toString().trim();

        } catch (Exception e) {
            return null;
        }

    }
}
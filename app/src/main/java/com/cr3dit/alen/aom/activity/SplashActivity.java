package com.cr3dit.alen.aom.activity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.controller.AsyncDbVersionCheck;
import com.cr3dit.alen.aom.controller.AsyncResponse;
import com.cr3dit.alen.aom.controller.DatabaseHelper;

import org.json.JSONArray;


import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity implements AsyncResponse{

    DatabaseHelper dbHelper;

    private static final String JSON_URL = "http://aomsys.ddns.net/dbinfo";
    private static final String JSON_ENTREPRENEUR = "http://aomsys.ddns.net/entrepreneur";
    private static final String JSON_ARTICLE = "http://aomsys.ddns.net/articles";

    private static final String JSON_CATEGORIES = "http://aomsys.ddns.net/categories";


    AsyncDbVersionCheck asyncTask=new AsyncDbVersionCheck(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        dbHelper=new DatabaseHelper(this);

        try {
    DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());
    SQLiteDatabase db = dbHelper.getReadableDatabase();
    db.close();
    }
        catch (SQLiteException ex){}

            if (isOnline()) {


            asyncTask.delegate = this;
            asyncTask.execute(JSON_URL,JSON_ARTICLE,JSON_ENTREPRENEUR,JSON_CATEGORIES);



        }
            else{
              Toast.makeText(this, "Device does not have internet access.",
            Toast.LENGTH_LONG).show();

        }

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        }, 700);
    }

    @Override
    public void processFinish(JSONArray output) {

        //translate http response to JSON array


    }


// ex http://stackoverflow.com/questions/12575068/how-to-get-the-result-of-onpostexecute-to-main-activity-because-asynctask-is-a
// ex to pass urls... param https://developer.android.com/reference/android/os/AsyncTask.html


    public boolean isOnline() {
    ConnectivityManager cm =
            (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = cm.getActiveNetworkInfo();
    return netInfo != null && netInfo.isConnectedOrConnecting();
}
}

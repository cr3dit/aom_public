package com.cr3dit.alen.aom.model;


/**
 * Created by alen on 22.01.2017..
 */

public class Category {
    String _idCategory, categoryName;

    public Category() {

    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String get_idCategory() {
        return _idCategory;
    }

    public void set_idCategory(String _idCategory) {
        this._idCategory = _idCategory;
    }
}




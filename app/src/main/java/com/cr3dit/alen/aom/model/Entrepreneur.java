package com.cr3dit.alen.aom.model;

/**
 * Created by alen on 19.01.2017..
 */

public class Entrepreneur {
    String _idEntrepreneur, entrepreneurName, entPic;
    public Entrepreneur() {

    }

    public String get_idEntrepreneur() {
        return _idEntrepreneur;
    }

    public void set_idEntrepreneur(String _idEntrepreneur) {
        this._idEntrepreneur = _idEntrepreneur;
    }
    public void setEntPic(String entPic){

        this.entPic=entPic;
    }
    public String getEntPic(){return  entPic;}

    public String getEntrepreneurName() {
        return entrepreneurName;
    }

    public void setEntrepreneurName(String entrepreneurName) {
        this.entrepreneurName = entrepreneurName;
    }
}

package com.cr3dit.alen.aom.model;

import android.content.Intent;

import com.cr3dit.alen.aom.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alen on 08.01.2017..
 */

public class Menu {
    private String menuTitle;
    private int menuPic;


    private Menu(String menuTitle, int menuPic) {

        this.menuPic = menuPic;
        this.menuTitle = menuTitle;


    }



    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public Integer getMenuPic() {
        return menuPic;
    }

    public void setMenuPic(Integer menuPic) {
        this.menuPic = menuPic;
    }
}







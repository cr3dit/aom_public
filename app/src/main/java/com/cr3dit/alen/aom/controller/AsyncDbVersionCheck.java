package com.cr3dit.alen.aom.controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.cr3dit.alen.aom.model.DbVersionCheck;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alen on 22.04.2017..
 */

public class AsyncDbVersionCheck extends AsyncTask<String, Void, JSONArray> {

    private Context mContext;
    
 public AsyncDbVersionCheck(Context context){
        mContext = context;
    }
    
public AsyncResponse delegate=null;
    DatabaseHelper dbHelper;
    DbVersionCheck dbVersionCheckObject;
    int dbVerLocal,artVerLocal,catVerLocal,entVerLocal;
    int dbVerServer,artVerServer,catVerServer,entVerServer;
    String artName,entrepreneurName,categoryName;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONArray doInBackground(String... params) {

        String param[]=new String[params.length];

        JSONArray jsonArray= null;

  dbHelper=new DatabaseHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        JSONParser parser = new JSONParser();
        dbVersionCheckObject =dbHelper.getDbVer();

        boolean updateArticle=false;
        boolean updateCategories=false;
        boolean updateEntrepreneurs=false;

        dbVerLocal = dbVersionCheckObject.getDbVer();

        for(int x=0;x<params.length;x++)
            {
                param[x] = parser.jsonData(params[x]);
                try {
                    // param[0] is db version check
                    jsonArray = new JSONArray(param[x]);
                    JSONObject jsonObject;

                    if(x==0){
                        jsonObject = jsonArray.getJSONObject(0);

                        artVerServer=Integer.parseInt(jsonObject.getString("article_version"));
                        entVerServer=Integer.parseInt(jsonObject.getString("entrepreneur_version"));
                        catVerServer=Integer.parseInt(jsonObject.getString("categories_version"));

                        artVerLocal=dbVersionCheckObject.getArtVer();
                        entVerLocal=dbVersionCheckObject.getEntVer();
                        catVerLocal=dbVersionCheckObject.getCatVer();

                        if (artVerLocal != artVerServer) {

                            updateArticle = true;
                            Log.d("UPDATEARTICLE", updateArticle + "");

                        }
                         if(entVerLocal!=entVerServer){

                            updateEntrepreneurs=true;
                            Log.d("UPDATEENT", updateEntrepreneurs + "");

                        }

                         if(catVerLocal!=catVerServer){

                            updateCategories=true;
                            Log.d("UPDATECAT", updateCategories + "");

                        }

                    }
                    else if(x==1&&updateArticle) {

                        dbHelper.deleteDataFromTable("articles");

                        for (int y = 0; y < jsonArray.length(); y++) {

                            jsonObject = jsonArray.getJSONObject(y);

                            dbHelper.addArticles(jsonObject.getString("_article_id"),jsonObject.getString("article_name"),0,Double.parseDouble(jsonObject.getString("article_price1")),0,jsonObject.getString("_entrepreneur_id"),jsonObject.getString("_category_id"),jsonObject.getString("article_picture"));

                            artName=jsonObject.getString("article_name");


                        }
                             dbHelper.updateDbVer("articles_version",artVerServer);
                                            //(string,int);    

                           updateArticle=false;
                    
                    
                    }
                    else if(x==2&&updateEntrepreneurs){
                        dbHelper.deleteDataFromTable("entrepreneurs");

                        for (int y = 0; y < jsonArray.length(); y++) {

                            jsonObject = jsonArray.getJSONObject(y);

                            dbHelper.addEntrepreneurs(jsonObject.getString("_entrepreneur_id"),jsonObject.getString("entrepreneur_name"),jsonObject.getString("entrepreneur_location"),jsonObject.getString("entrepreneur_picture"));
                            entrepreneurName=jsonObject.getString("entrepreneur_name");


                        }
                        dbHelper.updateDbVer("entrepreneurs_version",entVerServer);

                    }
                    else if(x==3&&updateCategories){
                        for (int y = 0; y < jsonArray.length(); y++) {

                            jsonObject = jsonArray.getJSONObject(y);

                            categoryName=jsonObject.getString("category_name");


                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;



    }

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        super.onPostExecute(jsonArray);

        Log.d("PARSERSHOWONLYLAST", jsonArray + "");

        //delegated result to processFinish method of interface
        delegate.processFinish(jsonArray);

    }
}


package com.cr3dit.alen.aom.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.controller.DatabaseHelper;
import com.cr3dit.alen.aom.model.Article;
import com.squareup.picasso.Picasso;

/**
 * Created by alen on 15.01.2017..
 */
public class ArticleDetailFragment extends Fragment {

     String URL="http://aomsys.ddns.net";

    public String ARTICLE_ID="";
    Article article;
    float updateQuantity;
    double basePrice;
    TextView articleName,articlePrice,quantity;
    ImageView articlePicture;
    SharedPreferences pref;
    CardView cv_addToBasketButton, cv_increment,cv_decrement;
    DatabaseHelper dbHelper;
    public ArticleDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        dbHelper=new DatabaseHelper(getActivity());
        pref = getActivity().getPreferences(0);
        ARTICLE_ID = pref.getString("ArticleClicked", "notClicked");
    return inflater.inflate(R.layout.fragment_detail,container,false);

    }

     public void onActivityCreated(final Bundle savedInstanceState){
         super.onActivityCreated(savedInstanceState);
         try {
             article = dbHelper.getArticleDetails(ARTICLE_ID);

         }
         catch (Exception e){e.printStackTrace();}
       getUiElements();

         basePrice =article.getPrice_1();

         articleName.setText(article.getName());
         articlePrice.setText(String.format("%.2f", basePrice));

            URL=URL+article.getArticlePic();

            Log.d("IMGAREURL", URL+"");

         if (article.getArticlePic()!=null){

             Picasso.with(getContext()).load(URL).into(articlePicture);
         }
         else {

             //todo set default picture
         }



         updateQuantity=Float.valueOf(quantity.getText().toString());


         cv_increment.setOnClickListener(new View.OnClickListener(){
             @Override
             public void onClick(View v) {
                 updateQuantity=updateQuantity+1;
                quantity.setText(updateQuantity+"");



                 articlePrice.setText( String.format("%.2f", basePrice*updateQuantity )+" KM");
             }
         });

         cv_decrement.setOnClickListener(new View.OnClickListener(){
             @Override
             public void onClick(View v) {
                 if (updateQuantity>1){
                 updateQuantity=updateQuantity-1;
                 quantity.setText(updateQuantity+"");


                     articlePrice.setText( String.format("%.2f", basePrice*updateQuantity )+" KM");

                 }}
         });


        cv_addToBasketButton.setOnClickListener(new View.OnClickListener(){
    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity(),"Article added to basket",Toast.LENGTH_SHORT).show();
        updateQuantity=Float.valueOf(quantity.getText().toString());

        dbHelper.insertIntoBasket(article.getID(),updateQuantity,"0001");


    }
});


    }
    public void getUiElements(){

        articleName=(TextView)getView().findViewById(R.id.txt_articleName);
        articlePrice=(TextView)getView().findViewById(R.id.txt_articlePrice);
        quantity=(TextView)getView().findViewById(R.id.txt_quantity);
articlePicture=(ImageView)getView().findViewById(R.id.img_entLogo);
        cv_addToBasketButton =(CardView)getView().findViewById(R.id.cv_addToBasketButton);
        cv_increment=(CardView)getView().findViewById(R.id.cv_increment);
        cv_decrement=(CardView)getView().findViewById(R.id.cv_decrement);


    }


}


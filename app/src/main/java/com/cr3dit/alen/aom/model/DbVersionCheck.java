package com.cr3dit.alen.aom.model;

/**
 * Created by alen on 22.04.2017..
 */

public class DbVersionCheck {
    int dbVer,artVer,entVer,catVer;

    public int getDbVer() {
        return dbVer;
    }

    public void setDbVer(int dbVer) {
        this.dbVer = dbVer;
    }

    public int getArtVer() {
        return artVer;
    }

    public void setArtVer(int artVer) {
        this.artVer = artVer;
    }

    public int getEntVer() {
        return entVer;
    }

    public void setEntVer(int entVer) {
        this.entVer = entVer;
    }

    public int getCatVer() {
        return catVer;
    }

    public void setCatVer(int catVer) {
        this.catVer = catVer;
    }

    public DbVersionCheck(){};



}

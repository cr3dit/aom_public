package com.cr3dit.alen.aom.controller;

import org.json.JSONArray;

/**
 * Created by alen on 22.04.2017..
 */

public interface AsyncResponse{
    void processFinish(JSONArray jsonArray);
}

package com.cr3dit.alen.aom.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.controller.ArticlesAdapter;
import com.cr3dit.alen.aom.controller.DatabaseHelper;
import com.cr3dit.alen.aom.model.Article;
import com.cr3dit.alen.aom.model.Entrepreneur;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by alen on 10.01.2017..
 */

public class EntrepreneurDetailFragment extends Fragment {
    DatabaseHelper dbHelper;
    String ENTREPRENEUR_CLICKED;
    SharedPreferences pref;
    List<Article> dataCollection;
    Entrepreneur entrepreneur;
    int articleSize;
    int counter;
    ArticlesAdapter adapter;
    TextView entName,statusText;
    ImageView entLogo;
    final private String URL="http://aomsys.ddns.net/";

    public EntrepreneurDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dbHelper = new DatabaseHelper(getActivity());

        View entFrag = inflater.inflate(R.layout.fragment_ent_detail, container, false);
        RecyclerView menuRecycler = (RecyclerView) entFrag.findViewById(R.id.recycler_view);

        pref = getActivity().getPreferences(0);
        ENTREPRENEUR_CLICKED = pref.getString("EntrepreneurClicked", "notClicked");
        Log.d("ENTREPRENEUR CLICKED", ENTREPRENEUR_CLICKED);

        dataCollection = dbHelper.getArticlesByEntrepreneur(Integer.parseInt(ENTREPRENEUR_CLICKED));

       // pref.edit().remove("EntrepreneurClicked").apply();

        articleSize = dataCollection.size();

        final String[] articleIds = new String[articleSize];
        final String[] articleNames = new String[articleSize];
        final String[] articlePic = new String[articleSize];

        final float[] articlePrice=new float[articleSize];
        counter = 0;
        for (Article articles : dataCollection) {
            articleNames[counter] = articles.getName();
            articleIds[counter] = articles.getID();
            articlePrice[counter]=articles.getPrice_1();
            articlePic[counter] = articles.getArticlePic();

            counter++;
        }

        final FragmentTransaction ft = getFragmentManager().beginTransaction();

        adapter = new ArticlesAdapter(getActivity(),articleNames,articlePrice,articlePic);

        menuRecycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        menuRecycler.setLayoutManager(layoutManager);


        adapter.setListener(new ArticlesAdapter.Listener() {
            public void onClick(int position) {

                pref = getActivity().getPreferences(0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("ArticleClicked", (articleIds[position]));
                editor.apply();
                ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                ft.addToBackStack(null);
                ft.replace(R.id.MainFrame, new ArticleDetailFragment(), "NewFragmentTag");
                ft.commit();

            }
        });

        return entFrag;

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        entrepreneur = dbHelper.getEntrepreneurDetails(ENTREPRENEUR_CLICKED);

        getUiElements();
        entName.setText(entrepreneur.getEntrepreneurName());

        Picasso.with(getActivity()).load(URL+entrepreneur.getEntPic()).into(entLogo);

        if(dataCollection.isEmpty()) {
            statusText.setVisibility(View.VISIBLE);
            statusText.bringToFront();
        }
        else{
            statusText.setVisibility(View.GONE);
        }


    }



    public void getUiElements() {

        entName = (TextView) getView().findViewById(R.id.txt_articleName);
        entLogo = (ImageView) getView().findViewById(R.id.img_entLogo);
        statusText=(TextView)getView().findViewById(R.id.textViewStatus);

    }


}
package com.cr3dit.alen.aom.controller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;
import com.squareup.picasso.Picasso;


/**
 * Created by alen on 08.01.2017..
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
    Context context;
    private String[] menuItems;
    private String[] menuPic;
    private Listener listener;
    final private String URL="http://aomsys.ddns.net/";



    public interface Listener {
        void onClick(int position);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    //constructor for 1 variable


    //constructor for 2 variables
    public MenuAdapter(Context context, String[] menuItems,String[] menuPic) {
        this.menuPic = menuPic;
        this.menuItems = menuItems;
        this.context=context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_entrepreneur_detail, parent, false);
        return new ViewHolder(cv);
    }


    public void onBindViewHolder(ViewHolder holder, final int position) {

        CardView cardView = holder.cardView;
        TextView textView = (TextView) cardView.findViewById(R.id.txt_articleName);
        ImageView imageView=(ImageView) cardView.findViewById(R.id.img_entLogo);

        textView.setText(menuItems[position]);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
       // imageView.setImageResource(menuPic[position]);

        Picasso.with(context).load(URL+menuPic[position]).into(imageView);
        Log.d("IMAGEURL", URL+menuPic[position]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return menuItems.length;
    }
}
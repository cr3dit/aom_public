package com.cr3dit.alen.aom.controller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;
import com.squareup.picasso.Picasso;


/**
 * Created by alen on 08.01.2017..
 */

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private String[] articleNames;
    private float[] price;
    private String[] articlePic;
    Context context;

    private Listener listener;
    final private String URL="http://aomsys.ddns.net/";

    public interface Listener {
        void onClick(int position);


    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    //constructor for 2 variables
    public ArticlesAdapter(Context context,String[] articleNames, float[] price,String[] articlePic) {
        this.articleNames = articleNames;
        this.price = price;
        this.articlePic=articlePic;
        this.context=context;

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_placeholder_articles, parent, false);
        return new ViewHolder(cv);
    }


    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        TextView textView = (TextView) cardView.findViewById(R.id.txt_articleName);
        ImageView imageView=(ImageView) cardView.findViewById(R.id.imageView);

        textView.setText(articleNames[position]);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        final TextView textView2 = (TextView) cardView.findViewById(R.id.txt_price);
        textView2.setText(price[position]+"");
if (articlePic!=null){
    Picasso.with(context).load(URL + articlePic[position]).into(imageView);
}
else {

    //todo set default picture
}
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);


                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return articleNames.length;
    }
}
package com.cr3dit.alen.aom.model;


/**
 * Created by alen on 12.01.2017..
 */

public class Article {

    //private variables
    String _id;
    String name;
    String entrepreneurId;

    public String getArticlePic() {
        return articlePic;
    }

    public void setArticlePic(String articlePic) {
        this.articlePic = articlePic;
    }

    String articlePic;
    double warehouse;
float price_1, price_2,quantity;


    public Article() {

    }




    public String getEntrepreneurId() {
        return entrepreneurId;
    }

    public void setEntrepreneurId(String entrepreneurId) {
        this.entrepreneurId = entrepreneurId;
    }


    public double getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(double warehouse) {
        this.warehouse = warehouse;
    }

    public float getPrice_1() {
        return price_1;
    }

    public void setPrice_1(float price_1) {
        this.price_1 = price_1;
    }

    public float getPrice_2() {
        return price_2;
    }

    public void setPrice_2(float price_2) {
        this.price_2 = price_2;
    }

    // getting name
    public String getName() {
        return this.name;
    }

    // setting name
    public void setName(String name) {
        this.name = name;
    }

    // getting ID
    public String getID() {

        return this._id;
    }

    // setting ID
    public void setID(String _id) {

        this._id = _id;
    }


    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public float getQuantity() {
        return quantity;
    }
}